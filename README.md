with chrome, in order to work around CORS errors, you need to use

`chrome --allow-file-access-from-files`

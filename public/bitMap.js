class bitMap {

    constructor(width=256,height=256,depth=4) {
        this.width=width
        this.height=height
        this.depth=depth
        this.data=new Uint8ClampedArray(width*height*depth);
        //
        this.target=''  // set par show, la target choisie devient le defaut
        }



    select( s = { from:[0,0], size:[0,0], layers:[0,1,2,3] }  ) {
        if (s.size[0]==0) s.size[0]=this.width
        if (s.size[1]==0) s.size[1]=this.height

        var R=new bitMap(s.size[0],s.size[1])
        R.depth=s.layers.length

        var startIdx=s.from[1]*this.width*this.depth + s.from[0]*this.depth
        var n=0
        for (let ligne=0;ligne<s.size[1];ligne++) {
            var lIdx=startIdx+ligne*this.width*this.depth
            for (let pixel=0;pixel<s.size[0];pixel++) {
                var pos=lIdx+pixel*this.depth
                for (var layer in s.layers) {
                    R.data[n++]=this.data[pos+s.layers[layer]]
                    }
                }
            }
        return R
        }


    lyxMatrix() {
        var D=[]
        for (var l=0;l<this.depth;l++) {
            D.push([])
            for (var y=0;y<this.height;y++) {
                D[l].push([])
                var dy=y*this.width*this.depth
                for (var x=0;x<this.width;x++) {
                    D[l][y].push(this.data[dy+x*this.depth+l])
                    }
                }
            }
        return D
        }

    yxlMatrix() {
        var D=[]
        for (var y=0;y<this.height;y++) {
            D.push([])
            var dy=y*this.width*this.depth
            for (var x=0;x<this.width;x++) {
                D[y].push([])
                for (var l=0;l<this.depth;l++) {
                    D[y][x].push(this.data[dy+x*this.depth+l])
                    }
                }
            }
        return D
        }



    loadFile(filePath,callback) {
        const image=document.createElement('img')
        image.onload=function() {  // le this de ce callBack : {mybm:le bm,myimg:l'image}

                                const cv=document.createElement('canvas')
                                this.mybm.width=this.myimg.width
                                cv.width=this.mybm.width
                                this.mybm.height=this.myimg.height
                                cv.height=this.mybm.height
                                cv.getContext('2d').drawImage(this.myimg,0,0)
                                this.mybm.data=cv.getContext('2d').getImageData(0,0,cv.width,cv.height).data
                                // call post load hook
                                callback()
                    }.bind({mybm:this,myimg:image})
        image.src=filePath
        }



    show(cv) {
        //use or set default target
        if (cv) this.target=cv
        else cv=this.target
        //
        cv.width=this.width
        cv.height=this.height
        const tmp=new ImageData(this.data,this.width,this.height)
        const tgt=cv.getContext('2d')
        tgt.putImageData(tmp,0,0)
        }

    static fromLYXmatrix(data) {
        const depth=data.length
        const height=data[0].length
        const width=data[0][0].length
        var bm=new bitMap(width,height,depth)
        for (var y=0;y<height;y++) {
            let lidx=y*width*depth
            for (var x=0;x<width;x++) {
                let pos=lidx+x*depth
                for (var l=0;l<depth;l++) {
                    bm.data[pos+l]=data[l][y][x]
                }
            }
        }
        return bm
    }

    static fromYXLmatrix(data) {
        const depth=data[0][0].length
        const height=data.length
        const width=data[0].length
        var bm=new bitMap(data[0].length,data.length,data[0][0].length)
        for (var y=0;y<height;y++) {
            let lidx=y*width*depth
            for (var x=0;x<width;x++) {
                let pos=lidx+x*depth
                for (var l=0;l<depth;l++) {
                    bm.data[pos+l]=data[y][x][l]
                }
            }
        }
        return bm
    }


    static fromImage(img) {
      const w=img.width
      const h=img.height
      const depth=4 //?
      const cv=document.createElement('canvas')
      var bm=new bitMap(width=w,height=f,depth=depth)
      cv.width=w;cv.height=h
      cv.getContext('2d').drawImage(this.myimg,0,0)
      bm.data=cv.getContext('2d').getImageData(0,0,cv.width,cv.height).data
      }


}


////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
function loadImages(filelist) {}
